# znet

zeroEngine.Networking Library

Part of zeroSDK

Based on Raknet & ASIO

## Features
 - independent module;
 - easy-to-use API alongside isolation;
 - lightweight;
 - platform-independent;
 - none-blocking i/o;
 - easy ECS-integration;
 - easy to configure with config-headers;

## Installation
CMake install supported

## Usage
Supports both STATIC and SHARED build-types

## Contributing
Contact me

## Authors and acknowledgment
c0de4un

## License
MIT

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

